/*
 * grunt-devutils
 * https://bitbucket.org/sleelin/devutils
 *
 * Copyright (c) 2015 S. Lee-Lindsay
 * Licensed under the GNU license.
 */

"use strict";

//var path = require("path");

module.exports = function(grunt) {
    var Path = require("path"),
        Builder = require("./lib/builder").init(grunt);

    grunt.registerMultiTask("devutils", "", function() {
        var done = this.async(),
            opts = this.options({
                modules: {
                    forms: "form.js",
                    components: "module.js"
                }
            });

        Object.keys(opts.modules).forEach(function(type) {
            if (grunt.util.kindOf(opts.modules[type]) == "string") {
                opts.modules[type] = (Path.extname(opts.modules[type]) == ".js" ? {module: opts.modules[type]} : {template: opts.modules[type]});
            }
        });

        this.files.forEach(function (f) {
            var modules = f.src.filter(isDir).filter(isModule).map(function (src) {
                    return {
                        src: src,
                        dest: f.dest,
                        name: Path.relative(opts.cwd, src)
                    }
                });

            // Copy unrelated files to destination
            grunt.file.expand({filter: "isFile"}, f.src.filter(function (filepath) {
                // Compare against identified modules to make sure we're not doubling up
                return modules.every(function (module) {
                    return ((filepath.indexOf(module.src) !== 0) && (!grunt.file.doesPathContain(filepath, module.src)));
                })
            }).filter(isDir).map(function (dir) {
                //TODO: better way of copying specified files
                return dir+"/**";
            })).forEach(function (file) {
                // Actually copy unrelated files
                grunt.file.copy(file, Path.join(f.dest, Path.relative(opts.cwd, file)));
            });

            // Build identified modules
            modules.map(getBuilder).forEach(function (builder) {
                builder.build(opts.index, opts.main);
            });
        });

        function isDir(filepath) {
            return grunt.file.isDir(filepath);
        }

        function isModule(filepath) {
            return Object.keys(opts.modules).some(function (module) {
                return grunt.file.isFile(filepath, opts.modules[module].module);
            });
        }

        function getBuilder(module) {
            var parts = module.name.split(Path.sep),
                type = parts.shift(),
                name = parts.join(Path.sep);

            if (type in opts.modules) {
                var filename = opts.modules[type].module;

                if (grunt.file.isMatch([opts.cwd, type, "*", filename].join(Path.sep), [module.src, filename].join(Path.sep))) {
                    return new Builder(module.src, module.dest, name, opts.modules[type]);
                } else {
                    //TODO: handle sub modules
                }
            }
        }

        done();
    });

};
