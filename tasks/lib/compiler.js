/*
 * grunt-devutils
 * https://bitbucket.org/sleelin/devutils
 *
 * Copyright (c) 2015 S. Lee-Lindsay
 * Licensed under the GNU license.
 */

"use strict";

var $ = require("cheerio"),
    beautify = require("js-beautify").html;

function FormCompiler(document) {
    this._document = $.load(document).root();
}

FormCompiler.prototype.get = function () {
    return beautify(this._document.toString());
};

FormCompiler.prototype.embed = function (source) {
    $("[data-embed-target]", this._document).append(source).attr("data-embed-target", null);

    return this;
};

FormCompiler.prototype.name = function (name) {
    var element = $("[data-ctc-app]", this._document);
    element.attr("data-ctc-app", name);

    return this;
};

FormCompiler.prototype.styles = function (styles) {
    var element = $("link[rel='stylesheet']", document).last();

    styles = (Array.isArray(styles) ? styles : [styles]);

    for (var i = 0; i < styles.length; ++i) {
        element = element.after("<link href='' rel='stylesheet' type='text/css' />").next().attr("href", styles[i]);
    }

    return this;
};

FormCompiler.prototype.scripts = function (scripts) {
    var element = $("script[src]", this._document).last();

    scripts = (Array.isArray(scripts) ? scripts : [scripts]);

    for (var i = 0; i < scripts.length; ++i) {
        element = element.after("<script></script>").next().attr("src", scripts[i]);
    }

    return this;
};

module.exports = FormCompiler;
