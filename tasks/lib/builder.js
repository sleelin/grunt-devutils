"use strict";

var Path = require("path"),
    PageCompiler = require("./compiler");

module.exports.init = function(grunt) {

    function Builder(src, dest, name, files) {
        this._src = src;
        this._dest = Path.join(dest, name);
        this._files = files;
    }

    Builder.prototype.build = function() {
        var target = this._files.target || this._files.module,
            baseName = Path.basename(target, Path.extname(target)),
            prependScript = (this._files.prepend && grunt.file.isFile(this._files.prepend) ? grunt.file.read(this._files.prepend) : ""),
            scripts = grunt.file.expand({cwd: this._src}, [this._files.module, "**/*.js"]),
            templates = grunt.file.expand({cwd: this._src}, "*/**/*.html"),
            styles = grunt.file.expand({cwd: this._src}, "**/*.css");

        this.copy(templates, "templates");
        this.merge(prependScript, scripts, target);
        this.merge("", styles, [baseName, "css"].join("."));
        this.compile(this._dest.split(Path.sep).pop(), baseName);
    };

    Builder.prototype.compile = function(name, dest) {
        if (this._files.index && this._files.template) {
            var content = grunt.file.read(Path.join(this._src, this._files.template)),
                module = [name, [dest, "js"].join(".")].join("/"),
                page = new PageCompiler(grunt.file.read(this._files.index)).name(name).embed(content);

            grunt.file.write(Path.join(this._dest, [dest, "html"].join(".")), page.scripts(module).get());
        }
    };

    Builder.prototype.copy = function(files, flatpath) {
        for (var i = 0, ilen = files.length; i < ilen; ++i) {
            var dest = (flatpath != undefined ? Path.join(this._dest, flatpath) : this._dest),
                name = (flatpath != undefined ? files[i].split(Path.sep).pop() : files[i]);

            grunt.file.copy(Path.join(this._src, files[i]), Path.join(dest, name));
        }
    };

    Builder.prototype.merge = function(contents, files, dest) {
        for (var i = 0, ilen = files.length; i < ilen; ++i) {
            contents += newline(grunt.file.read(Path.join(this._src, files[i])));
        }

        if (dest && contents) grunt.file.write(Path.join(this._dest, dest), contents);
        return contents;
    };

    function newline(string) {
        string = grunt.util.normalizelf(string);

        if (string.lastIndexOf(grunt.util.linefeed) !== (string.length - grunt.util.linefeed.length)){
            string += grunt.util.linefeed;
        }

        return string;
    }

    return Builder;

};
